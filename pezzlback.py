#! /usr/bin/env python

import os
import sys
import subprocess as sp
import re
import argparse
import datetime
import glob
import platform
import getpass
import time
import ConfigParser
import json


def sync(source,dest,options,excludepatterns="",includes="*",excluderemainder="*"):
    print('*** Syncing folders : '+source+' ==> '+dest )
    cmd = sp.Popen(
                [
                'rsync',
                source,
                dest
                ]
                +
                options
                +
                excludepatterns
                +
                includes
                +
                excluderemainder
                ).communicate()
    return

parser = argparse.ArgumentParser(description='Pezzlback backup scripts')
parser.add_argument('-c', '--config', type=str,metavar='source directory containing .pezzlconfig file')
parser.add_argument('-s', '--source', type=str,metavar='source directory')
parser.add_argument('-d', '--destination', type=str,metavar='destination directory')
parser.add_argument('-v','--verbose', action='store_true', help='show verbose output')
parser.add_argument('-go', action='store_true', help='really do it (dry run is default)')
parser.add_argument('-del','--delete', action='store_true',help='delete files from destination that are removed in source')
parser.add_argument('-lex','--listdefaultexcludes', action='store_true',help='list all directories that are exlcuded')

excludepatterns = []
excludepatterns.append("--exclude"); excludepatterns.append("System Volume Information") 
excludepatterns.append("--exclude"); excludepatterns.append(".Trash-1000") 
excludepatterns.append("--exclude"); excludepatterns.append("RECYCLER") 
excludepatterns.append("--exclude"); excludepatterns.append("Recycled") 
excludepatterns.append("--exclude"); excludepatterns.append("$RECYCLE.BIN") 

includes = [] # as default include everything
includes.append("--include"); includes.append("*") 

excluderemainder = [] # as default include everything
excluderemainder.append("--exclude"); excluderemainder.append("*") 

options = []
options.append("-a") # -a, --archive               archive mode; equals -rlptgoD (no -H,-A,-X)
#options.append("-vv") # -v, --verbose               increase verbosity
options.append("-i") # -i, --itemize-changes       output a change-summary for all updates
options.append("-h") # -h, --human-readable        output numbers in a human-readable format
options.append("-n") # -n, --dry-run               perform a trial run with no changes made
#options.append("--info=progress2") #   --progress             
#options.append("--delete") #    --delete           delete extraneous files from destination dirs

if len(sys.argv) == 1:
    parser.print_help()
    sys.exit(1)

args = parser.parse_args()

# configuration print and exit
if args.listdefaultexcludes:
    print("Directories that are excluded on all levels:")
    print("\n".join(excludepatterns[1::2]))
    sys.exit()


# preparing 
if args.verbose:
    options.append("-vv")
    options.append("--progress")
else:
    options.append("--info=progress2")
if args.go:
    options.remove("-n") 
if args.delete:
    options.append("--delete")
     
# set source and destination  
source = ""
destination = ""    
if args.source and args.destination and not args.config:
    source = args.source
    destination = args.destination
if args.config:
    configfilename = os.path.join(args.config,".pezzlback")
    if os.path.isfile(configfilename):
        ini = ConfigParser.ConfigParser(allow_no_value=True)
        ini.read(configfilename)
        
        source = json.loads(ini.get("BackupConfig","Source"))
        destination = json.loads(ini.get("BackupConfig","Destination"))

        excludelist = json.loads(ini.get("ExcludePatterns","dirs"))
        excludepatterns = []
        for item in excludelist:
            excludepatterns.append("--exclude"); excludepatterns.append(item) 

        includedirs = json.loads(ini.get("Backups","dirs"))            
        includes = []
        for item in includedirs:
            includes.append("--include"); includes.append(os.path.join(item, '','***'))    
        includefiles = json.loads(ini.get("Backups","files"))            
        for item in includefiles:
            includes.append("--include"); includes.append(item)                 
    else:
        print("Could not find .pezzlback file in folder("+args.config+")") 
        sys.exit() 
# add trailing slashes
source = os.path.join(source, '')
destination = os.path.join(destination, '')



# check paths
if not os.path.isdir(source):
    print("Invalid Source Path:")
    print("Source: " + source)
    sys.exit()
if not os.path.isdir(destination):
    print("Invalid Destination Path:")  
    print("Destination: " + destination)    
    sys.exit()

# print summary
print("Your options are:")
print(options)
print("Source: " + source)
print("Destination: " + destination)
if args.config:
    print("Files to Backup:")
    print("\n".join(includes[1::2]))
    print("Patterns that are excluded on all levels:")
    print("\n".join(excludepatterns[1::2]))

# safety   
if  "-n" not in options:
    # source filename (stored to source directory)
    options.append("--log-file="+source+time.strftime("%Y%m%d_%H%M%S")+"_Pezzlback.log")
    print("This is not a dry run, please enter password") 
    pwd = "go"
    p = getpass.getpass()
    if p != pwd:
        print("wrong password") 
        sys.exit()  
# safety   
if  "-n" not in options and "--delete" in options:
    print("This is a DELETE run that deletes files from the destination that are not present in the source, please enter password") 
    pwd = "delete"
    p = getpass.getpass()
    if p != pwd:
        print("wrong password") 
        sys.exit()            
    
# final call    
sync(source,destination,options,excludepatterns,includes,excluderemainder)
sys.exit()
